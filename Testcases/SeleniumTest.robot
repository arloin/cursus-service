*** Settings ***
Resource             ../Keywords/SeleniumKeywords.robot
Suite Teardown       Close All Browsers
Documentation        Keyword documentation: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
...
...                  Kijk goed of er keywords zijn die je kan gebruiken in de Keywords/SeleniumKeywords.robot file

*** Variable ***
${SELENIUM_DELAY}    1
${TEST_URL}          http://cursusclient-ravenclaw-hco-kza-cursus.apps.prod.am5.hotcontainers.nl/cursussen
${GROUP_NR}          1

${CURSUS_NAAM}       Dit Is Een Cursus Naam

*** Test Cases ***
Opdracht 0: Google Test
    # Als deze test werkt dan werkt RobotFramework goed. Verwijder deze als je begint.
    Open Browser In Jenkins             https://www.google.com
    Wait Until Page Contains Element    name=q                    timeout=5

Opdracht 1: Login in de cursusclient
    Open Browser In Jenkins             ${TEST_URL}
    Click Element                       id=login                  
    Wait Until Page Contains Element    id=logout                 timeout=5