package nl.kzaconnected.cursus.repository;

import nl.kzaconnected.cursus.model.Dao.Attitude;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AttitudeRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private AttitudeRepository attitudeRepository;

    @Test
    public void findByAttitudeShouldReturnAttitudeWithId() {
        Attitude attitude = Attitude.builder().attitude("Attitude1").build();
        entityManager.persist(attitude);
        entityManager.flush();
        Attitude found = attitudeRepository.findByAttitude(attitude.getAttitude());
        assertThat(found.getId()).isNotNull();
        assertThat(found.getAttitude()).isEqualTo(attitude.getAttitude());
    }
}